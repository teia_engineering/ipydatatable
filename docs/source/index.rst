
ipydatatable
=====================================

Version: |release|

A Custom Jupyter Widget Library


Quickstart
----------

To get started with ipydatatable, install with pip::

    pip install ipydatatable

or with conda::

    conda install ipydatatable


Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Installation and usage

   installing
   introduction

.. toctree::
   :maxdepth: 1

   examples/index


.. toctree::
   :maxdepth: 2
   :caption: Development

   develop-install


.. links

.. _`Jupyter widgets`: https://jupyter.org/widgets.html

.. _`notebook`: https://jupyter-notebook.readthedocs.io/en/latest/
